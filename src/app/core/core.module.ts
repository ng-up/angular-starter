import { NgModule, Optional, SkipSelf } from '@angular/core';
import { AppComponent } from './app-component/app.component';
import { AppRoutingModule } from './app-routing.module';
import { RouterModule } from '@angular/router';

// not used in production
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { storeFreeze } from 'ngrx-store-freeze';
export const metaReducers: MetaReducer<any>[] = !environment.production
  ? [storeFreeze]
  : [];

import { StoreModule, MetaReducer } from '@ngrx/store';
import * as fromStore from './store';
import { EffectsModule } from '@ngrx/effects';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { environment } from '../../environments/environment';
import { AuthModule } from '../auth/auth.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    AppRoutingModule,
    StoreModule.forRoot(fromStore.reducers, { metaReducers }),
    EffectsModule.forRoot([fromStore.RouterEffects]),
    AuthModule,
    StoreRouterConnectingModule,
    !environment.production ? StoreDevtoolsModule.instrument() : []
  ],
  exports: [AppComponent, RouterModule]
})
export class CoreModule {
  constructor(
    @Optional()
    @SkipSelf()
    parentModule: CoreModule
  ) {
    if (parentModule) {
      throw new Error('CoreModule is already loaded. Import only in AppModule');
    }
  }
}
