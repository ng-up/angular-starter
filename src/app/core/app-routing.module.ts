import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from '../auth/guards';

export const DEFAULT_PAGE = 'dashboard';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: DEFAULT_PAGE,
    canActivate: [AuthGuardService]
  },
  {
    path: DEFAULT_PAGE,
    canActivate: [AuthGuardService],
    loadChildren: '../exemplefeat/module#ExampleModule'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
