import { NgModule } from '@angular/core';
import { DumbComponent } from './dumb.component';
import { SharedModule } from '../shared/shared.module';
import { RouterModule, Routes } from '@angular/router';

export const routes: Routes = [
  {
    path: 'feat',
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'testtt' },
      { path: 'testtt', component: DumbComponent }
    ]
  }
];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  declarations: [DumbComponent],
  providers: []
})
export class ExampleModule {}
