import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-dumb-comp',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `<h1>HEY YOUR AUTH ?</h1>`
})
export class DumbComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
