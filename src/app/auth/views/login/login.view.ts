import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  // Input,
  Output,
  EventEmitter
} from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-login-view',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['login.view.scss'],
  template: `
  <div class="login-view-container">
    <mat-card class="login-view-card">
      <app-auth-form (submitted)="LoginFormSubmitted($event)">
        <h1>Welcome, please login</h1>
        <button
          mat-raised-button
          color="primary"
          class="auth-form__action"
        >Submit
        </button>
      </app-auth-form>
    </mat-card>
  </div>`
})
export class LoginViewComponent implements OnInit {
  // @Input()
  // data: any;

  @Output()
  submittedForm = new EventEmitter<FormGroup>();

  constructor() {}

  ngOnInit() {}

  LoginFormSubmitted(fg: FormGroup) {
    this.submittedForm.emit(fg);
  }
}
