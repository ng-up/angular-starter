import { AuthGuardService } from './auth.guard';

export const guards = [AuthGuardService];

export * from './auth.guard';
