import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Store, select } from '@ngrx/store';
import { selectAuth } from '../store';
import { AuthState } from '../models/auth.model';
import { Go } from '../../core/store';

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(private store: Store<AuthState>) {}

  canActivate(): Observable<boolean> {
    return this.store.pipe(
      select(selectAuth),
      map(auth => {
        console.log({ auth });
        if (!auth.isAuthenticated) {
          console.log('GO LOGIN');
          this.store.dispatch(new Go({ path: ['auth/login'] }));
        }
        return auth.isAuthenticated;
      })
    );
  }

  //   canActivate() {
  //     return this.authService.authState
  //       .map((user) => {
  //         if (!user) {
  //           this.router.navigate(['/auth/login']);
  //         }
  //         return !!user;
  //       });
  //   }
}
