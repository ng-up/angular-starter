import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import * as fromComponents from './components';
import * as fromContainers from './containers';
import * as fromViews from './views';
import * as fromGuards from './guards';
import * as fromServices from './services';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { AuthEffects, AuthReducer } from './store';

import { SharedModule } from '../shared/shared.module';
import { AuthRoutingModule } from './auth-routing.module';

@NgModule({
  declarations: [
    ...fromComponents.components,
    ...fromViews.views,
    ...fromContainers.containers
  ],
  providers: [...fromGuards.guards, ...fromServices.services],
  imports: [
    SharedModule,
    ReactiveFormsModule,
    AuthRoutingModule,
    StoreModule.forFeature('auth', AuthReducer),
    EffectsModule.forFeature([AuthEffects])
  ]
})
export class AuthModule {}
