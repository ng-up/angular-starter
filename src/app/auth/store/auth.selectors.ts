import { createSelector } from '@ngrx/store';
import { getAuthState } from './auth.reducer';
import { AuthState } from '../models/auth.model';

export const selectAuth = createSelector(
  getAuthState,
  (state: AuthState) => state
);
