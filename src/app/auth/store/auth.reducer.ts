import { AuthActions, AuthActionTypes } from './auth.actions';
import { AuthState } from '../models/auth.model';
import { ActionReducerMap, createFeatureSelector } from '@ngrx/store';

export const initialState: AuthState = {
  isAuthenticated: false
};

export function AuthReducer(
  state: AuthState = initialState,
  action: AuthActions
): AuthState {
  switch (action.type) {
    case AuthActionTypes.LOGIN:
      return { ...state, isAuthenticated: true };

    case AuthActionTypes.LOGOUT:
      return { ...state, isAuthenticated: false };

    default:
      return state;
  }
}

export const getAuthState = createFeatureSelector<AuthState>('auth');
