import { AuthComponent } from './auth/auth.container';

export const containers = [AuthComponent];

export * from './auth/auth.container';
