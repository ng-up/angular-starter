import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AuthState } from '../../models/auth.model';
import { Store } from '@ngrx/store';
import { ActionAuthLogin } from '../../store';

@Component({
  selector: 'app-auth',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `<app-login-view (submittedForm)="loginFormSubmitted($event)"></app-login-view>`
})
export class AuthComponent implements OnInit {
  constructor(private store: Store<AuthState>) {}

  ngOnInit() {}

  loginFormSubmitted(form: FormGroup) {
    // check data
    this.store.dispatch(new ActionAuthLogin());
  }
}
